package com.testwork.user.dao.impl;

import com.testwork.user.dao.UserDao;
import com.testwork.user.model.User;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class UserDaoImpl extends
        HibernateDaoSupport implements UserDao {

    public void addUser(User user) {

        getHibernateTemplate().save(user);

    }

    public List<User> findAllUsers() {

        return getHibernateTemplate().find("from User");

    }
}