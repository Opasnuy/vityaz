package com.testwork.user.dao;
 
import com.testwork.user.model.User;

import java.util.List;

public interface UserDao {
 
	void addUser(User user);
 
	List<User> findAllUsers();
 
}