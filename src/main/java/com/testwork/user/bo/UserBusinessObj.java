package com.testwork.user.bo;
 
import com.testwork.user.model.User;

import java.util.List;
 
public interface UserBusinessObj {
 
	void addUser(User user);
 
	List<User> findAllUser();
 
}