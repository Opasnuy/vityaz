package com.testwork.user.bo.impl;
 
import com.testwork.user.bo.UserBusinessObj;
import com.testwork.user.dao.UserDao;
import com.testwork.user.model.User;

import java.util.List;
 
public class UserBusinessObjImpl implements UserBusinessObj {
 
	UserDao userDao;
 
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
 
	public void addUser(User user){
 
		userDao.addUser(user);
 
	}
 
	public List<User> findAllUser(){
 
		return userDao.findAllUsers();
	}
}